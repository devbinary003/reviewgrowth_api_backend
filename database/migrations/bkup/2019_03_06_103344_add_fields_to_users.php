<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('title')->nullable()->after('active_code');
            $table->text('description')->nullable()->after('title');
            $table->tinyInteger('isonline')->default('0')->nullable()->after('description');
            $table->integer('llhid')->default('0')->nullable()->after('isonline');
            $table->string('user_field_1', 191)->nullable()->after('llhid');
            $table->string('user_field_2', 191)->nullable()->after('user_field_1');
            $table->string('user_field_3', 191)->nullable()->after('user_field_2');
            $table->string('user_field_4', 191)->nullable()->after('user_field_3');
            $table->string('user_field_5', 191)->nullable()->after('user_field_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('isonline');
            $table->dropColumn('llhid');
            $table->dropColumn('user_field_1');
            $table->dropColumn('user_field_2');
            $table->dropColumn('user_field_3');
            $table->dropColumn('user_field_4');
            $table->dropColumn('user_field_5');
        });
    }
}
