<html>
<head>
</head>
<body style="font-family: Arial; font-size: 12px;">
<div>
	<p style="font-size: 24px;">Hii, {{ $firstname }}</p>
    <p>
        You have requested a password reset, please follow the link below to reset your password.
    </p>
    <p>
        Please ignore this email if you did not request a password change.
    </p>

    <p>
            <?php $password_reset_link = env('REVIEWGROWTH_URL').'/resetpassword/?email='.$email;?>
            <a href="{{$password_reset_link}}">
            Follow this link to reset your password.
        </a>
    </p>
</div>
</body>
</html>