<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body style="margin: 0px;font-family: Roboto;">
    <div style="width: 600px;margin: auto;">
      <div style="float: left;width: 100%;">
<div style="background-image: url('./reviewgrowthapi/public/images/headerbg.jpg');color: #fff;text-align: center;padding: 30px 0px;background-repeat: no-repeat;background-size: cover;background-position: center;">
<h1 style="margin:0px;font-weight: normal;">{{$business_name}}</h1>
<p style="margin: 0px;">{{$business_address}}</p>
</div>
<div style="background-image: url('./reviewgrowthapi/public/images/headerbg.jpg');background-repeat:no-repeat;background-size:cover;padding: 50px 30px;box-sizing: border-box;float: left;width: 100%;min-height: 550px;"">
<h1 style="margin: 0px;font-weight: normal;font-size: 18px;">Hi,{{$reviewer_name}}</h1>
<p style="margin-top:8px;">{{$sc_emsg}}</p>
<ul style="padding:0px;">
  <li style="display: inline;float: left;width: 100%;margin-bottom: 20px;"><i class="fa fa-paper-plane" style="color: #4185f3;font-size: 24px;padding-right: 10px;float: left;width: 30px;"></i>Step 1: Click the button below to open live google review.<br/>
    <a href="https://search.google.com/local/writereview?placeid={{$business_page_id}}" target="_blank"><img src='./reviewgrowthapi/public/images/satisfied_client-2.jpg' alt="google" style="margin-top:10px;"></a></li>
  <li style="display: inline;float: left;width: 100%;margin-bottom: 20px;"><i class="fa fa-paper-plane" style="color: #4185f3;font-size: 24px;padding-right: 10px;float: left;width: 30px;"></i>Step 2: Sign in to your gmail or Google Account if needed?<br/>
    <img src='./reviewgrowthapi/public/images/googlebtn.jpg' alt="google" style="margin-top:10px;"></li>
  <li style="display: inline;float: left;width: 100%;margin-bottom: 20px;"><i class="fa fa-paper-plane" style="color: #4185f3;font-size: 24px;padding-right: 10px;float: left;width: 30px;"></i>Step 3: Submit and you are all done.
    <p style="float: left;width: 100%;margin-top: 10px;background: #f8f3f3;padding: 20px;border: 1px solid #e9e3e3;box-sizing: border-box;line-height: 25px;font-size: 16px;color: #484848;">{{$sc_enote}}</p></li>
</ul>
  </div>
</div>
<div style="background-image: url(./reviewgrowthapi/public/images/headerbg.jpg);color: #fff;text-align: center;padding: 20px 0px;float: left;width: 100%;background-repeat: no-repeat;background-size: cover;background-position: center;">
<p style="margin-bottom: 0px;">Medex Diagnostic & Treatment Center works<br/>13 Howard Court, Saint paul, MN 55104</p>
  </div>
</div>
  </body>
</html>