<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
      <title>review</title>
      <style type="text/css">
         body{
            width:100%;
            background-color:#ffffff;
            margin:0;
            padding:0;
            -webkit-font-smoothing:antialiased;
            mso-margin-top-alt:0;
            mso-margin-bottom-alt:0;
            mso-padding-alt:0 0 0 0;
         }
         p,h1,h2,h3,h4{
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            padding-bottom:0;
         }
         span.preheader{
            display:none;
            font-size:1px;
         }
         html{
            width:100%;
         }
         table{
            font-size:12px;
            border:0;
         }
         .menu-space{
            padding-right:25px;
         }
         @media only screen and (max-width:640px){
            body{
               width:auto !important;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .main{
               width:440px !important;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .two-left{
               width:420px !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .full{
               width:100% !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .alaine{
               text-align:center;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .menu-space{
               padding-right:0;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .banner{
               width:438px !important;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .menu{
               width:438px !important;
               margin:0 auto;
               border-bottom:#e1e0e2 solid 1px;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .date{
               width:438px !important;
               margin:0 auto;
               text-align:center;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .two-left-inner{
               width:400px !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .menu-icon{
               display:block;
            }
         }  
         @media only screen and (max-width:640px){
            body[yahoo] .two-left-menu{
               text-align:center;
            }
         }  
         @media only screen and (max-width:479px){
            body{
               width:auto !important;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .main{
               width:310px !important;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .two-left{
               width:300px !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .full{
               width:100% !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .alaine{
               text-align:center;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .menu-space{
               padding-right:0;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .banner{
               width:308px !important;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .menu{
               width:308px !important;
               margin:0 auto;
               border-bottom:#e1e0e2 solid 1px;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .date{
               width:308px !important;
               margin:0 auto;
               text-align:center;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .two-left-inner{
               width:280px !important;
               margin:0 auto;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .menu-icon{
               display:none;
            }
         }  
         @media only screen and (max-width:479px){
            body[yahoo] .two-left-menu{
               width:310px !important;
               margin:0 auto;
            }
         }
      </style>
   </head>
   <body yahoo="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="width:100%; background-color:#ffffff; margin:0; padding:0; -webkit-font-smoothing:antialiased; mso-margin-top-alt:0; mso-margin-bottom-alt:0; mso-padding-alt:0 0 0 0;">
      
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center">
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td align="center" valign="top" bgcolor="#FFFFFF">
                <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                  <tr>
                    <td align="center" valign="middle" bgcolor="#15A1C4"><a href="https://www.reviewgrowth.com/" target="_blank"><img src="{{url('/public')}}/images/maillogo3.png" width="383" height="84" border="0" alt="maillogo3.png"></a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="center">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" valign="top" bgcolor="#FFFFFF">
              <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                <tr>
                  <td align="center" valign="top" bgcolor="#FFFFFF" style="background:#ffffff;padding-top:20px;padding-bottom:20px;">
                    <table width="565" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left">
                      <tr>
                        <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:35px;color:#000000;padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:30px;font-weight:bold;">Confirm Your Email Address</td>
                      </tr>
                      <tr>
                        <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#000000;padding-bottom:10px;padding-left:10px;padding-right:10px;padding-top:30px;">
                          <span style="font-size:17px; color:#676767; font-weight:bold;">Hi {{$firstname}} {{$lastname}},</span>
                          <br>
                          <br>
                          Thank you so much for your registration.</td>
                        </tr>
                        <tr>
                          <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:15px;color:#000000;padding-bottom:30px;padding-left:10px;padding-right:10px;">
                            <table width="565" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left">
                              <tr>
                                <td align="center" valign="top" bgcolor="#f8f8f8">
                                  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353;padding-top:10px;padding-bottom:20px;">Tap the button below to confirm your email address.
                                        If you didn't create an account with ReviewGrowth, you can safely delete this email.</td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                                <tr>
                                  <td height="45" align="center" valign="top" bgcolor="#FFFFFF" style="padding-bottom:20px; padding-top:10px;">
                                    <table width="200" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left">
                                      <tr>
                                        <td align="center" valign="top">
                                          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                              <td align="center" valign="top" bgcolor="#15A1C4" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:20px;color:#ffffff;padding-bottom:10px;padding-top:10px;font-weight:bold;border-radius: 25px;"><a href="{{ env('REVIEWGROWTH_URL') }}/login/?active_code={{$active_code}}" target="_blank" style="color:#FFFFFF;text-decoration:none;">Click to verify email</a>
                                            </td>
                                          </tr>
                                        </table>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="45" align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353;padding-top:20px;padding-bottom:20px;">If that doesn't work, copy and paste the following link in your browser.<br><a href="https://www.reviewgrowth.com" target="_blank">{{ env('REVIEWGROWTH_URL') }}/login/?active_code={{$active_code}}</a>
                              </td>
                            </tr>
                            <tr>
                              <td align="left" valign="top" bgcolor="#FFFFFF" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:17px;color:#535353;padding-top:20px;padding-bottom:20px;"> <span style="font-weight:bold">The ReviewGrowth Team.</span>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="center" bgcolor="#FFFFFF">
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="center" valign="top" bgcolor="#FFFFFF">
            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
              <tr>
                <td align="center" valign="top" bgcolor="#15A1C4" style="background:#15A1C4;padding-top:20px;padding-bottom:20px;">
                  <table width="540" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left">
                    <tr>
                      <td height="45" align="center" valign="top" style="font-family:'Myriad Pro', Arial, Helvetica, sans-serif;font-size:13px;color:#ffffff;padding-bottom:20px;padding-left:10px;padding-right:20px;">
                        <a href="mailto:unsubscribe@reviewgrowth.com" style="color:#FFFFFF;text-decoration:none;">Unsubscribe</a>
                        <br>
                        <br>
                        ReviewGrowth © Copyright 2020</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
     
   </body>
</html>

