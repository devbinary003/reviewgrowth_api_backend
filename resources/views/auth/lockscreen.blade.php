@extends('layouts.default')

@section('content')

<!-- <div class="hold-transition lockscreen" data-gr-c-s-loaded="true" cz-shortcut-listen="true"> -->
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a><b>Admin</b>LTE</a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name">John Doe</div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="./public/images/user1-128x128.jpg" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" class="form-control" placeholder="password">

        <div class="input-group-btn">
          <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Enter your password to retrieve your session
  </div>
  <div class="text-center">
    <a href="./login">Or sign in as a different user</a>
  </div>
  <div class="lockscreen-footer text-center">
    Copyright © 2019 <b><a href="" class="text-black">BinaryData</a></b><br>
    All rights reserved
  </div>
</div>

<style>
  header {display: none;}
  .main-sidebar {display: none;}
  .lockscreen-name {
    text-align: center;
    font-weight: 600;
}
</style>
<!-- </div> -->
@endsection