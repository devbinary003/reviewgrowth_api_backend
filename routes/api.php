 <?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api','cors']], function ($router) {
  Route::post('register', 'Api\AuthController@register');
  Route::post('login', 'Api\AuthController@login');
  Route::post('logout', 'Api\AuthController@logout');
  Route::post('forgetpassword', 'Api\AuthController@forgetpassword');
  Route::post('resetpassword', 'Api\AuthController@Resetpassword');
  Route::get('reset/password/{email}', 'Api\AuthController@passwordReset');
  Route::post('/account/activation/{active_code}', 'Api\AuthController@userActivation');
  Route::get('livereviewcron', 'Api\LivereviewcronController@livereviewcron');
});

Route::group(['middleware' => ['api','jwt.verify','cors']], function ($router) {
  Route::post('refresh', 'Api\AuthController@refresh');
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'user'], function ($router) {

  Route::post('findbusiness/{id}', 'Api\UserController@updateFindbusiness');
  Route::post('businessinfo/{id}', 'Api\UserController@updateBusinessinfo');
  Route::post('customizereview/{id}', 'Api\UserController@updateCustomizereview');

  Route::post('billing/{id}', 'Api\UserController@updateBilling');
  Route::get('detail/{id}', 'Api\UserController@getuserDetails');

  Route::post('accountsettings/{id}', 'Api\UserController@updateAccountsettings');
  Route::post('systemsettings/{id}', 'Api\UserController@updateSystemsettings');
  Route::post('editreviewlink/{id}', 'Api\UserController@updateReviewlink');

  Route::post('checkemail', 'Api\UserController@checkemail');
  Route::post('checkusername', 'Api\UserController@checkusername');

});

Route::group(['middleware' => ['api','jwt.verify','cors'], 'prefix' => 'user'], function() {
  Route::post('checkuserlogin', 'Api\UserController@checkuserlogin');
});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'review'], function () {
  
  Route::get('internalreviewsdef/{id}', 'Api\BusinessreviewController@getInternalreviewsDef');
  Route::get('internalreviews/{id}', 'Api\BusinessreviewController@getInternalreviews');

  Route::get('satisfiedreviewsdef/{id}', 'Api\BusinessreviewController@getSatisfiedreviewsDef');
  Route::get('satisfiedreviews/{id}', 'Api\BusinessreviewController@getSatisfiedreviews');

  Route::get('unsatisfiedreviewsdef/{id}', 'Api\BusinessreviewController@getUnsatisfiedreviewsDef');
  Route::get('unsatisfiedreviews/{id}', 'Api\BusinessreviewController@getUnsatisfiedreviews');
  
  Route::get('livereviewsdef/{id}', 'Api\BusinessreviewController@getLivereviewsDef');
  Route::get('livereviews/{id}', 'Api\BusinessreviewController@getLivereviews');

  Route::get('lvsatisfiedreviewsdef/{id}', 'Api\BusinessreviewController@getLvSatisfiedreviewsDef');
  Route::get('lvsatisfiedreviews/{id}', 'Api\BusinessreviewController@getLvSatisfiedreviews');

  Route::get('lvunsatisfiedreviewsdef/{id}', 'Api\BusinessreviewController@getLvUnsatisfiedreviewsDef');
  Route::get('lvunsatisfiedreviews/{id}', 'Api\BusinessreviewController@getLvUnsatisfiedreviews');
  
  Route::get('liverating/{id}', 'Api\BusinessreviewController@getLiveRating');
  Route::get('toplivereviews/{id}', 'Api\BusinessreviewController@getTopLiveReviews');
  Route::get('all/{id}', 'Api\BusinessreviewController@getAll');
  /*Route::get('satisfied/{id}', 'Api\BusinessreviewController@getSatisfied');
  Route::get('unsatisfied/{id}', 'Api\BusinessreviewController@getUnsatisfied');*/

  Route::get('mylivereviews/{id}', 'Api\BusinessreviewController@getMylivereviews');
  Route::get('myinternalreviews/{id}', 'Api\BusinessreviewController@getMyinternalreviews');
  
  Route::get('searchreviewdef/{id}', 'Api\BusinessreviewController@getSearchReviewDef');
  Route::get('searchreview/{id}', 'Api\BusinessreviewController@getSearchReview');
  
  Route::get('getbusinessdetails/{name}', 'Api\BusinessreviewController@getbusinessdetails');

});

Route::group(['middleware' => ['api','cors'], 'prefix' => 'businesslocation'], function () {

  Route::get('detail/{id}', 'Api\BusinesslocationsController@getBusinessLocation');
  Route::post('addfindbusiness/{id}', 'Api\BusinesslocationsController@AddFindBusiness');
  Route::post('addbusinessinfo/{id}', 'Api\BusinesslocationsController@AddBusinessInfo');
Route::post('addcustomizereview/{id}', 'Api\BusinesslocationsController@AddCustomizeReview');
  

  Route::post('addnewbusinesslocation/{id}', 'Api\BusinesslocationsController@AddNewBl');
  Route::get('bllists/{id}', 'Api\BusinesslocationsController@getBlLists');
  Route::get('bldetail/{id}', 'Api\BusinesslocationsController@getBlDetail');
  Route::post('updatebl/{id}', 'Api\BusinesslocationsController@updateBl');
  Route::post('getautofilldetails', 'Api\BusinesslocationsController@getAutofillDetails');

  Route::get('filterbl/{id}', 'Api\BusinesslocationsController@getFilterBl');
  Route::get('singlebldetail/{id}', 'Api\BusinesslocationsController@getSingleBlDetail');
  
});


Route::group(['middleware' => ['api','jwt.verify','cors'],'prefix' => 'businesssetting'], function(){
    Route::post('update/{id}', 'Api\BusinessSettingController@updatebusiness');
});

Route::group(['middleware' => ['api','cors'],'prefix' => 'businesssetting'], function(){
    Route::get('detail/{id}', 'Api\BusinessSettingController@businessshow');
});


Route::group(['middleware' => ['api','jwt.verify','cors'],'prefix' => 'errorlog'], function(){
    Route::get('lists', 'Api\ErrorLogController@errorLists');
    Route::post('search', 'Api\ErrorLogController@errorSearch');
    Route::get('detail/{id}', 'Api\ErrorLogController@geterrorDetails');
});
