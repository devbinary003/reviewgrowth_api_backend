<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Businesssetting extends Model
{
	protected $table = 'businesssetting'; 
    protected $fillable = [
        'id', 'business_name', 'business_address', 'email', 'phone', 'website', 'logo', 'facebook_link', 'twitter_link', 'instagram_link', 'linkedin_link', 'google_link', 'default_currency', 'language', 'payment_mode', 'paypal_test_email', 'paypal_live_email', 'stripe_test_pub_key', 'stripe_test_sec_key', 'stripe_live_pub_key', 'stripe_live_sec_key', 'google_map_url', 'ex1', 'ex2', 'ex3', 'ex4', 'ex5', 'ex6', 'ex7', 'ex8', 'ex9', 'ex10', 'created_at', 'updated_at'
    ];

    
}
