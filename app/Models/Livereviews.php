<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Livereviews extends Model
{

	protected $table = 'live_reviews';

    protected $fillable = [
        'id', 'live_review_id', 'live_review_reviewer', 'live_review_datetime', 'live_review_rating', 'live_review_text', 'live_review_url', 'live_review_language', 'business_user_id', 'business_place_id', 'business_slug', 'status', 'is_deleted', 'is_complete', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];
    
}