<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Sendresetpassword extends Mailable
{
    use Queueable, SerializesModels;
    public $user_email;
    // public $user_dcrypt_password;
    public $user_first_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_email, $user_first_name)
    {
        $this->user_email = $user_email;
        // $this->user_dcrypt_password = $user_dcrypt_password;
        $this->user_first_name = $user_first_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.resetpassword');
    }
}