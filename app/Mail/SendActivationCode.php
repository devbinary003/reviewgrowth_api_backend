<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendActivationCode extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $firstname;
    public $lastname;
    public $active_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstname, $lastname, $email,$active_code)
    {
        $this->email = $email;
        $this->lastname = $lastname;
        $this->firstname = $firstname;
        $this->active_code = $active_code;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        return $this->from('info@reviewgrowth.com','Reviewgrowth')
                    ->subject("Account Activation Code")
                    ->view('mails.userActivate');
    }
}