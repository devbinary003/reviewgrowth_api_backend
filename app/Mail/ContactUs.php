<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;
    public $user_name;
    public $user_email;
    public $contact_subject;
    public $text;
    public $phone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name,$user_email,$contact_subject,$text,$phone)
    {
        //
        $this->user_name = $user_name;
        $this->user_email = $user_email;
        $this->contact_subject = $contact_subject;
        $this->text = $text;
        $this->phone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.contactus');
    }
}
