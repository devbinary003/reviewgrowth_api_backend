<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailSatisfiedReview extends Mailable
{
    use Queueable, SerializesModels;
    public $sc_emsg;
    public $sc_ebtnclr;
    public $sc_enote;
    public $business_user_email;
    public $business_user_name;
    public $business_name;
    public $business_address;
    public $business_page_id;
    public $reviewer_name;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sc_emsg, $sc_ebtnclr, $sc_enote, $business_user_email, $business_user_name, $business_name, $business_address, $business_page_id, $reviewer_name)
    {
        $this->sc_emsg = $sc_emsg;
        $this->sc_ebtnclr = $sc_ebtnclr;
        $this->sc_enote = $sc_enote;
        $this->business_user_email = $business_user_email;
        $this->business_user_name = $business_user_name;
        $this->business_name = $business_name;
        $this->business_address = $business_address;
        $this->business_page_id = $business_page_id;
        $this->reviewer_name = $reviewer_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dev2.bdpl@gmail.com','ReviewGrowth')->view('mails.satisfiedmail');
    }
}