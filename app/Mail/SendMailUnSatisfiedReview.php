<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUnSatisfiedReview extends Mailable
{
    use Queueable, SerializesModels;
    public $unsc_emsg;
    public $sc_ebtnclr;
    public $business_user_email;
    public $business_user_name;
    public $business_name;
    public $business_address;
    public $business_page_id;
    public $reviewer_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($unsc_emsg, $sc_ebtnclr, $business_user_email, $business_user_name, $business_name, $business_address, $business_page_id, $reviewer_name)
    {
        $this->unsc_emsg = $unsc_emsg;
        $this->sc_ebtnclr = $sc_ebtnclr;
        $this->business_user_email = $business_user_email;
        $this->business_user_name = $business_user_name;
        $this->business_name = $business_name;
        $this->business_address = $business_address;
        $this->business_page_id = $business_page_id;
        $this->reviewer_name = $reviewer_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('dev2.bdpl@gmail.com','ReviewGrowth')->view('mails.unsatisfiedmail');
    }
}