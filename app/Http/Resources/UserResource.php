<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;

class UserResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return array
   */
  public function toArray($request)
  {
    return [
      'id' => $this->id,
      'username' => $this->username,
      'email' => $this->email,
      'firstname' => $this->firstname,
      'lastname' => $this->lastname,
      'fullname' => $this->fullname,
      'phone' => $this->phone,
      'address' => $this->address,
      'city' => $this->city,
      'state' => $this->state,
      'country' => $this->country,
      'zipcode' => $this->zipcode,
      'lat' => $this->lat,
      'long' => $this->long,
      'profilepic' => $this->profilepic,
      'security_ques' => $this->security_ques,
      'security_ans' => $this->security_ans,
      'role' => $this->role,
      'device' => $this->device,
      'browser' => $this->browser,
      'ipaddress' => $this->ipaddress,
      'status' => $this->status,
      'isdeleted' => $this->isdeleted,
      'isapproved' => $this->isapproved,
      'logins' => $this->logins,
      'last_login' => $this->last_login,
      'created_by' => $this->created_by,
      'updated_by' => $this->updated_by,
    ];  
  }
}
