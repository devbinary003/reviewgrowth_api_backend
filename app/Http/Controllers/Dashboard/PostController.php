<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    //
    public  function getList(){
    	return view('postlist.post-list');
    }

    public function postEdit(){
    	return view('postlist.post-edit');
    }
    public function getSinglePost(){
    	return view('postlist.post-view');
    }
}
