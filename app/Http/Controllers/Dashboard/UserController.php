<?php
namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function getUserList(){
    	return view('userlist.user-list');
    }
    public function EditUserList(){
    	return view('userlist.user-edit');
    }
     public function getSingleUser(){
    	return view('userlist.user-view');
    }

}
