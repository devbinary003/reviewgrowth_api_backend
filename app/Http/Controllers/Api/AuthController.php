<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use App\Mail\ForgetPassword;
use App\Mail\Sendresetpassword;
use App\Mail\SendActivationCode;

use App\Models\User;
use App\Models\Loginhistory;
use App\Http\Resources\UserResource;

use DB;
use Mail;
use Log;
use Image;
use URL;
use JWTAuth;
use JWTFactory;
use Carbon\Carbon; 
use Session;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;
use Illuminate\Support\Facades\Storage;

class AuthController extends Controller
{

  public function register(Request $request){

    try{

         $rules = [
            // 'username' => 'unique:users',
             'email' => 'unique:users',
         ];

         $messages = [
            // 'username.unique'   =>  'That user name is already in use.',
             'email.unique'   =>  'That email address is already in use.',
         ];

         $validator = Validator::make($request->all(), $rules, $messages);
         if ($validator->fails()) 
         { 
             return response()->json(['status'=>false,'message'=>'Email address already in use','error'=>$validator->errors(),'data'=>'']);
         }

        $Input =  [];
        $Input['username'] = trim($request->email);
        $Input['email'] = trim($request->email);
        $Input['password'] = trim(Hash::make($request->password));
        $Input['hdpwd'] = trim($request->password);
        $Input['firstname'] = ucfirst(trim($request->firstname));
        $Input['lastname'] = ucfirst(trim($request->lastname));
        $Input['fullname'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
        $Input['phone'] = '';
        $Input['address_line_1'] = '';
        $Input['address_line_2'] = '';
        $Input['city'] = '';
        $Input['state'] = '';
        $Input['country'] = '';
        $Input['zipcode'] = '';
        $Input['profilepic'] = '';
        $Input['role'] = trim($request->role);
        $Input['device'] = isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null;
        $Input['browser'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $Input['ipaddress'] = isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null;
        $Input['active_code'] = strtolower(Str::random(30));
        $Input['isonline'] = '0';
        $Input['llhid'] = '0';
        $Input['number_of_locations'] = '';
        $Input['item_qnty'] = '';
        $Input['total_sum'] = '';
        $Input['sc_emsg'] = "Thank you so much for your kind words. We really appreciate it and would love for others to read it too. Help us out by making your review go live on Google!";
        $Input['sc_ebtnclr'] = '#333';
        $Input['sc_enote'] = "Thanks again for taking the time and effort to make sure your kind words are live on Google. It really means a lot to us and we thank you for your patronage. See you soon!";
        $Input['unsc_emsg'] = "Thank you very much for your honest feedback. We're so sorry to hear that you were not 100% satisfied at our business. Customer satisfaction is so important to us and we'd love the opportunity to improve. Please contact us during our regular business hours so we can assist you in any way. We sincerely thank you once again for your patronage. Have a wonderful day!";
        $Input['customer_id'] = '';
        $Input['subscription_id'] = '';
        $Input['plan_id'] = '';
        $Input['amount'] = '';
        $Input['subscription_status'] = '';
        $Input['current_period_start'] = '';
        $Input['current_period_end'] = '';
        $Input['start_date'] = '';
        $Input['card_number'] = '';
        $Input['card_exp_month'] = '';
        $Input['exp_year'] = '';
        $Input['name_on_card'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
        $Input['reg_step_1'] = '';
        $Input['reg_step_2'] = '';
        $Input['reg_step_3'] = '';
        $Input['reg_step_4'] = ''; 
        $Input['isprofilecomplete'] = ''; 
        $Input['status'] = '1';
        $Input['isdeleted'] = '0';
        $Input['isapproved'] = '1';
        $Input['isactivationcomplete'] = '0';
        $Input['logins'] = '0';
        $Input['created_by'] = '';
        $Input['updated_by'] = '';

        $auth_user = User::create($Input);
        if($auth_user) {
            $token = JWTAuth::fromUser($auth_user);
            //$user = new UserResource($auth_user); 
            $user = User::where('id', $auth_user->id)->first()->toArray();
            $from_email = env('MAIL_FROM_ADDRESS');
            $firstname = $user['firstname'];
            $lastname = $user['lastname'];
            $email = $request->email;
            $active_code = $user['active_code'];
            $sendemail = Mail::to($email)->send(new SendActivationCode($firstname,$lastname,$email,$active_code));
            
            $credentials = $request->only('email', 'password');
            if ($token = $this->guard()->attempt($credentials)) {
            $auth_user = $this->guard()->user(); 
            $logindata = [
                'user_id' => $auth_user->id,
                'login_time' => Carbon::now(),
                'browser' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
                'operating_system' => isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null,
                'ip_address'  => isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null,
                'status'     => '1',
                'deleted' => '0',
            ];
            $lhId = Loginhistory::create($logindata);
            User::where('id', $auth_user->id)->update(['logins' => $auth_user->logins + 1, 'last_login' => Carbon::now(), 'isonline' => '1', 'llhid' => $lhId->id]);
        
            return response()->json(['status'=>true,'message'=>'User login successfully','error'=>'','token'=>$token,'data'=>$user]);   

            } else {
             return response()->json(['status'=>false,'message'=>'User Doesn`t Exist With This Email','error'=>'Invalid credentials']);
            }

            return response()->json(['status'=>true,'message'=>'User created successfully A Email Verfication link Is Sent To Your Email Account Please Verify Your Account','error'=>'','token'=>$token,'data'=>$user,'mailsend'=>$sendemail]);
            
            } else {
                return response()->json(['status'=>false,'message'=>'Sorry fail to create user. Please try again.','error'=>'','data'=>'']); 
            }

    } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

}

public function login(Request $request)
{   
    try{

        $login_type = filter_var($request->email, FILTER_VALIDATE_EMAIL ) 
        ? 'email' 
        : 'username';

        $request->merge([ $login_type => $request->email ]);
        $credentials = $request->only($login_type, 'password');

        if ($token = $this->guard()->attempt($credentials)) {
            $auth_user = $this->guard()->user(); 

            $logindata = [
                'user_id' => $auth_user->id,
                'login_time' => Carbon::now(),
                'browser' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
                'operating_system' => isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null,
                'ip_address'  => isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null,
                'status'     => '1',
                'deleted' => '0',
            ];

            $lhId = Loginhistory::create($logindata);
            User::where('id', $auth_user->id)->update(['logins' => $auth_user->logins + 1, 'last_login' => Carbon::now(), 'isonline' => '1', 'llhid' => $lhId->id]);
            $user = User::where('id', $auth_user->id)->where('isdeleted',$auth_user->isdeleted)->first()->toArray();
            if($user['isdeleted'] == 1){
             return response()->json(['status'=>false,'message'=>'User Doesn`t Exist With This Email','error'=>'Invalid credentials']);
        } else {
            
            return response()->json(['status'=>true,'message'=>'User login successfully','error'=>'','token'=>$token,'data'=>$user]);

          /*if($user['status'] == 0){
            return response()->json(['status'=>false,'message'=>'Please Activate Your Account First Form Your Email','error'=>'']);
           } else {
            return response()->json(['status'=>true,'message'=>'User login successfully','error'=>'','token'=>$token,'data'=>$user]);
          }*/
        }
    }

return response()->json(['status'=>false,'message'=>'Please Enter Valid Password or Username','error'=>'Invalid credentials','data'=>'']);

} catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
} catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
}
}

public function logout()
{   
    $auth_user = $this->guard()->user();
    User::where('id', $auth_user->id)->update(['isonline' => '0']);
    Loginhistory::where('id', $auth_user->llhid)->update(['logout_time' => Carbon::now()]);
    $this->guard()->logout();
    return response()->json(['status'=>true,'message'=>'Successfully logged out','error'=>'','data'=>''], 200);
}

public function store(Request $request)
{
    if ($request->hasFile('profile_image')) {

        foreach($request->file('profile_image') as $file){

                        //get filename with extension
            $filenamewithextension = $file->getClientOriginalName();

                        //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                        //get file extension
            $extension = $file->getClientOriginalExtension();

                        //filename to store
            $filenametostore = $filename.'_'.uniqid().'.'.$extension;

            Storage::put('public/profile_images/'. $filenametostore, fopen($file, 'r+'));
            Storage::put('public/profile_images/thumbnail/'. $filenametostore, fopen($file, 'r+'));

                        //Resize image here
            $thumbnailpath = public_path('storage/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(400, 150, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            $smallthumbnailpath = public_path('storage/profile_images/thumbnail/small/'.$filenametostore);
            Image::make($thumbnailpath)->resize(200, 200)->save($smallthumbnailpath);
        }
                    //return redirect('images')->with('status', "Image uploaded successfully.");
    }
}

public function forgetpassword(Request $request)
{  
   try{
      $user_email = $request->email;
      $user_data = User::WHERE('email', $user_email)->WHERE('status', '1')->WHERE('isdeleted','0')->first();
      if(!empty($user_data)){
        $firstname = $user_data->firstname;
        $lastname= $user_data->lastname;
        $email=$user_email;
        $sendemail = Mail::to($email)->send(new ForgetPassword($firstname,$lastname,$email));
        return response()->json(['status'=>true,'message'=>'Your password reset link sent to your email!','data'=>$sendemail]); 
    } else {
        return response()->json(['status'=>false,'message'=>'User does not exist!']);
    }
} catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
} catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
}    
}

public function Resetpassword(Request $request)
{  
    try{
      $user_email = $request->email;
      $new_password = $request->password;
      $user_dcrypt_password = trim(Hash::make($new_password));
      $update_data = User::where('email', $user_email)->where('isdeleted','0')->update(['password' => $user_dcrypt_password]);
      if($update_data){
        $user_data = User::WHERE('email', $user_email)->first();
        $user_first_name = $user_data->firstname;
        $sendemail = Mail::to($user_email)->send(new Sendresetpassword($user_email, $user_first_name));
        return response()->json(['status'=>true,'message'=>'Your New password sent your email!']); 
    } else {
        return response()->json(['status'=>false,'message'=>'Record Not found!']);
    }
} catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
} catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
}    

}

public function userActivation(Request $request,$active_code){
    try{
        $user = User::where('active_code',$active_code)->where('isdeleted','0')->first();
        if(!empty($user)){
          $Input =  [];
          $Input['isactivationcomplete'] = '1';
          $Input['updated_at'] = date('Y-m-d h:i:s');
          $update = $user->update($Input);
          if($update){
            $user = User::where('id', $user->id)->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Your account has been successfully activated ','data'=>$user], 200);  
         } else {
            return response()->json(['status'=>false,'message'=>'Your account activation has been failed','data'=>$user], 200);
        }

    } else {
       return response()->json(['status'=>false,'message'=>'Sorry user does not exist'], 200);
   }

} catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
} catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
}

}
public function passwordReset($email){
    $user = User::where('email',$email)->where('isdeleted','0')->where('status','1')->first();
    if(!empty($user)){
        echo "User Details found";
    }
    else{
        echo "User Details Not found";
    }
}

public function refresh()
{
    return $this->respondWithToken($this->guard()->refresh());
}

protected function respondWithToken($token)
{
    return response()->json([
        'access_token' => $token,
        'token_type' => 'bearer',
        'expires_in' => $this->guard()->factory()->getTTL() * 999
    ]);
}

public function guard()
{
    return Auth::guard();
}

}
