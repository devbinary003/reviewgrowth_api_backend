<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use App\Models\Businesslocations;
use App\Models\Businessreview;
use App\Models\Livereviews;
use App\Http\Resources\UserResource;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use DB;
use Mail;
use Log;
use Image;
use URL;
use JWTAuth;
use JWTFactory;
use Carbon\Carbon; 

use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;
//use Illuminate\Support\Facades\Storage;

class LivereviewcronController extends Controller
{ 

  public function livereviewcron() {
    try{
      $msg = "First line of text\nSecond line of text";
      $msg = wordwrap($msg,70);
      $send = mail("dev1.bdpl@gmail.com","My subject",$msg);
      if($send) {
        echo 'Mail sent';
      } else {
        echo 'fail to send';
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

}
