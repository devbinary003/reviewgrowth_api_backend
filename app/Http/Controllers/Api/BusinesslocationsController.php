<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use App\Models\Businesslocations;
use App\Models\Businessreview;
use App\Models\Livereviews;
use App\Http\Resources\UserResource;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use DB;
use Mail;
use Log;
use Image;
use URL;
use JWTAuth;
use JWTFactory;
use Carbon\Carbon; 

use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;
//use Illuminate\Support\Facades\Storage;

class BusinesslocationsController extends Controller
{ 

  public function getBusinessLocation(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $isBusinesslocation = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        if($isBusinesslocation != 0) {
          $Businesslocation = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
          $user = User::where('id', $id)->first()->toArray();
          return response()->json(['status'=>true,'message'=>'Business location detail','error'=>'','data'=>$Businesslocation,'user'=>$user,'locationCount'=>$isBusinesslocation], 200);
        } else {
          return response()->json(['status'=>false,'message'=>'Business location not found','error'=>'','data'=>''], 200);
        }
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function AddFindBusiness(Request $request, $id) {
    try{
      $isUser = User::find(trim($id));
      if ($isUser) {
        $Input =  [];
        $InputUser =  [];
        $businessName = urlencode(trim($request->find_business_location));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
        if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {
          $business_page_id = $data['candidates'][0]['place_id'];
          $business_name = $data['candidates'][0]['name'];
          $chkBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('user_id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($chkBusiness == 0) {
                $Input['find_business_location'] = trim($request->find_business_location);
                $Input['business_page_id'] = $data['candidates'][0]['place_id'];
                $Input['business_address'] = $data['candidates'][0]['formatted_address'];
                $Input['business_name'] = $data['candidates'][0]['name'];
              //  $Input['business_rating'] = $data['candidates'][0]['rating'];
                $Input['lat'] = trim($request->lat);
                $Input['lng'] = trim($request->lng);
              if( trim($request->bl_id) != '') {
                $Input['updated_by'] = $id;
              Businesslocations::where('id', trim($request->bl_id))->update($Input);
              $Businesslocation = Businesslocations::where('id', trim($request->bl_id))->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
                return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
              } else {
                $Input['user_id'] = $id;
                $Input['business_rating'] = '';
                $Input['business_logo'] = '';
                $Input['phone'] = '';
                $Input['business_review_link'] = '';
                $Input['facebook_link'] = '';
                $Input['twitter_link'] = '';
                $Input['linkedin_link'] = '';
                $Input['instagram_link'] = '';
                $Input['client_satisfaction'] = '';
                $Input['created_by'] = $id;
                $data = Businesslocations::create($Input);
                if($data->id) {
                  $InputUser['reg_step_1'] = '1';
                  $InputUser['updated_by'] = $id;
                  User::where('id', $id)->update($InputUser);
                  $Businesslocation = Businesslocations::where('id', $data->id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
                  return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
                } else {
                  return response()->json(['status'=>false,'message'=>'Sorry fail to save data saved successfully.','error'=>'','data'=>''], 200);
                }
              }
            } else {
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
            }
        } else {
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>''], 200);
        }
      } else {
        return response()->json(['status'=>false,'message'=>'User not found.','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }

public function AddBusinessInfo(Request $request, $id) {
    try{
      $user_id = trim($request->user_id);
      $isUser = User::find($user_id);
      if ($isUser) {
        $Input =  [];
        $InputUser =  [];
          $business_page_id = trim($request->business_page_id);
          $business_name = trim($request->business_name); // $data['candidates'][0]['name'];
          $chkBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('user_id', '!=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($chkBusiness == 0) {
            $isBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('user_id', '=', $user_id)->where('id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
            if($isBusiness == 0) {
              $Input['business_name'] = trim($request->business_name); 
              $Input['phone'] = trim($request->phone);
              $Input['updated_by'] = $user_id;
              Businesslocations::where('id', $id)->update($Input);
              $number_of_locations = trim($request->number_of_locations);
              if ($number_of_locations != '') {
                    if($number_of_locations == 1) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_FnmzVv6VFQIgWq';
                    } elseif($number_of_locations == 2) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn01drULw2c7t';
                    } elseif($number_of_locations == 3) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn0ZbdDhZOAVL';  
                    } elseif($number_of_locations == 4) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn1HhexTiEK7E';
                    } elseif($number_of_locations == 5) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn2EDDErBCSLC';
                    } elseif($number_of_locations == 6) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn30KnfkLDjma';
                    } elseif($number_of_locations == 7) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn4mSV3mCePvu';
                    } elseif($number_of_locations == 8) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn4ezUcgKXvV2';
                    } elseif($number_of_locations == 9) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn59W1wSSckf6';
                    } elseif($number_of_locations == 10) {
                      $item_qnty = $number_of_locations - 1;
                      $total_sum = 47 + ($item_qnty * 10);
                      $planId = 'plan_Fnn6gaNt82a4o5';
                    } else {
                      $item_qnty = '';
                      $total_sum = '';
                      $planId = '';
                    }
                $InputUser['number_of_locations'] = trim($request->number_of_locations);
                $InputUser['item_qnty'] = $item_qnty;
                $InputUser['total_sum'] = $total_sum;
                $InputUser['plan_id'] = $planId;
              }
              $InputUser['firstname'] = ucfirst(trim($request->firstname));
              $InputUser['lastname'] = ucfirst(trim($request->lastname));
              $InputUser['fullname'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
              $InputUser['name_on_card'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
              $InputUser['reg_step_2'] = '1';
              $InputUser['updated_by'] = $user_id;
            User::where('id', $user_id)->update($InputUser);
            $Businesslocation = Businesslocations::where('id', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
            } else {
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
              }
            } else {
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
            }
        
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function AddCustomizeReview(Request $request, $id) {
    try{
      $user_id = trim($request->user_id);
      $isUser = User::find($user_id);
      if ($isUser) {
        $Input =  [];
        $InputUser =  [];
        $business_review_link = trim($request->business_review_link);
        $chkBrl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('user_id', '!=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        if($chkBrl == 0) {
        $isBurl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('user_id', '=', $user_id)->where('id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($isBurl == 0) {
            $Input['business_review_link'] = trim($request->business_review_link);

                if(trim($request->facebook_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->facebook_link))) {
                      $urlFb = "https://" . trim($request->facebook_link);
                      $Input['facebook_link'] = $urlFb;
                    } else {
                      $Input['facebook_link'] = trim($request->facebook_link);
                    }
                }

                if(trim($request->twitter_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->twitter_link))) {
                      $urlTwt = "https://" . trim($request->twitter_link);
                      $Input['twitter_link'] = $urlTwt;
                    } else {
                      $Input['twitter_link'] = trim($request->twitter_link);
                    }
                }

                if(trim($request->linkedin_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->linkedin_link))) {
                      $urlLnkdin = "https://" . trim($request->linkedin_link);
                      $Input['linkedin_link'] = $urlLnkdin;
                    } else {
                      $Input['linkedin_link'] = trim($request->linkedin_link);
                    }
                }
                
                if(trim($request->instagram_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->instagram_link))) {
                      $urlInsta = "https://" . trim($request->instagram_link);
                      $Input['instagram_link'] = $urlInsta;
                    } else {
                      $Input['instagram_link'] = trim($request->instagram_link);
                    }
                }

            /*$Input['facebook_link'] = trim($request->facebook_link);
            $Input['twitter_link'] = trim($request->twitter_link);
            $Input['linkedin_link'] = trim($request->linkedin_link);
            $Input['instagram_link'] = trim($request->instagram_link);*/

            $Input['client_satisfaction'] = trim($request->client_satisfaction);
            $Input['updated_by'] = $user_id;
            Businesslocations::where('id', $id)->update($Input);

            $InputUser['reg_step_3'] = '1';
            $InputUser['updated_by'] = $user_id;
            User::where('id', $user_id)->update($InputUser);
            $Businesslocation = Businesslocations::where('id', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
          } else {
            return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200);
          }
        } else {
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200);
            }
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }


  public function AddNewBl(Request $request, $id) {
    try{
      $isUser = User::find(trim($id));
      if ($isUser) {
        $Input =  [];
        $InputUser =  [];
        $user_id = trim($id);
        $businessName = urlencode(trim($request->find_business_location));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
      if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {
          $business_page_id = $data['candidates'][0]['place_id'];
          $business_name = trim($request->business_name); // $data['candidates'][0]['name'];
          $chkBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('user_id', '!=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($chkBusiness == 0) {
            $isBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('user_id', '=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
            if($isBusiness == 0) {
            $business_review_link = trim($request->business_review_link);
            $chkBrl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('user_id', '!=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
            if($chkBrl == 0) {
              $isBurl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('user_id', '=', $user_id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
              if($isBurl == 0) {
                $Input['user_id'] = $id;
                $Input['find_business_location'] = trim($request->find_business_location);
                $Input['business_page_id'] = $data['candidates'][0]['place_id'];
                $Input['business_address'] = $data['candidates'][0]['formatted_address'];
                $Input['business_name'] = trim($request->business_name);
                // $data['candidates'][0]['name'];
                // $Input['business_rating'] = $data['candidates'][0]['rating'];
                $Input['lat'] = trim($request->lat);
                $Input['lng'] = trim($request->lng);
                $Input['phone'] = trim($request->phone);
                $Input['business_rating'] = '';
                $Input['business_logo'] = '';
                $Input['business_review_link'] = trim($request->business_review_link);

                if(trim($request->facebook_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->facebook_link))) {
                      $urlFb = "https://" . trim($request->facebook_link);
                      $Input['facebook_link'] = $urlFb;
                    } else {
                      $Input['facebook_link'] = trim($request->facebook_link);
                    }
                }

                if(trim($request->twitter_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->twitter_link))) {
                      $urlTwt = "https://" . trim($request->twitter_link);
                      $Input['twitter_link'] = $urlTwt;
                    } else {
                      $Input['twitter_link'] = trim($request->twitter_link);
                    }
                }

                if(trim($request->linkedin_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->linkedin_link))) {
                      $urlLnkdin = "https://" . trim($request->linkedin_link);
                      $Input['linkedin_link'] = $urlLnkdin;
                    } else {
                      $Input['linkedin_link'] = trim($request->linkedin_link);
                    }
                }
                
                if(trim($request->instagram_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->instagram_link))) {
                      $urlInsta = "https://" . trim($request->instagram_link);
                      $Input['instagram_link'] = $urlInsta;
                    } else {
                      $Input['instagram_link'] = trim($request->instagram_link);
                    }
                }

                /*$Input['facebook_link'] = trim($request->facebook_link);
                $Input['twitter_link'] = trim($request->twitter_link);
                $Input['linkedin_link'] = trim($request->linkedin_link);
                $Input['instagram_link'] = trim($request->instagram_link);*/

                $Input['client_satisfaction'] = trim($request->client_satisfaction);
                $Input['created_by'] = $id;
                $data = Businesslocations::create($Input);
                if($data->id) {
                $Businesslocation = Businesslocations::where('id', $data->id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

                $TotalRatings = 0;
                $per_page = '10';
                $business_page_id = $Businesslocation->business_page_id;
                $user_id = $Businesslocation->user_id;
                $business_review_link = $Businesslocation->business_review_link;
                $myblid = $Businesslocation->id;

                $urlTop5 = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
                    $jsonTop5 = file_get_contents($urlTop5);
                    $dataTop5 = json_decode($jsonTop5, true);
               
                if($dataTop5['status']=='OK') { 
                  if(array_key_exists("user_ratings_total", $dataTop5['result'])){
                    $TotalRatingsCount = $dataTop5['result']['user_ratings_total'];
                    if( $TotalRatingsCount > 200 ) {
                      $TotalRatings = '200';
                    } else {
                      $TotalRatings = $dataTop5['result']['user_ratings_total'];
                    }
                    $totalPage = ceil( $TotalRatings / $per_page);
                    for($index=$totalPage;$index > 0;$index-=1) {
                      $from = ($index - 1) * 10;
                      $this->saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid);
                    }
                  } 
                }   

                return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
                } else {
                  return response()->json(['status'=>false,'message'=>'Sorry fail to save data saved successfully.','error'=>'','data'=>''], 200);
                }
              } else {
                return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200);
              }
            } else {
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200); 
            }
           } else {
             return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
           } 
          } else {
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
          }
        } else {
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>''], 200);
        }
      } else {
        return response()->json(['status'=>false,'message'=>'User not found.','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }

public function saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid) {
    try{
         $lvUrl = "https://wextractor.com/api/v1/reviews?offset=".$from."&id=".$business_page_id."&auth_token=".env('WEX_AUTH_TOKEN');
          $lvJson = file_get_contents($lvUrl);
          $lvData = json_decode($lvJson, true);
          
          if( count($lvData['reviews']) > 0 ) {
            $lvreviews = array_reverse($lvData['reviews']);
            foreach($lvreviews as $lvreview) {
              if($lvreview['text']!='') {
                $live_review_text = $lvreview['text'];
              } else {
                $live_review_text = 'No review was written for this submission.';
              }
              DB::table('live_reviews')->insert(
              ['live_review_id' => $lvreview['id'], 'live_review_reviewer' => $lvreview['reviewer'], 'live_review_datetime' => $lvreview['datetime'], 'live_review_rating' => $lvreview['rating'], 'live_review_text' => $live_review_text, 'live_review_url' => $lvreview['url'], 'live_review_language' => $lvreview['language'], 'business_user_id' => $user_id, 'business_place_id' => $business_page_id, 'business_slug' => $business_review_link]
             );
            }
            DB::table('businesslocations')
                ->where('id', $myblid)
                ->update(['lrvstatus' => '1']);
          }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function getBlLists(Request $request, $id) {
    try{
        $isUser = User::find($id);
        if ($isUser) {
          $isBusinesslocation = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($isBusinesslocation != 0) {
            $Businesslocation = Businesslocations::where('user_id', '=', $id)
                                ->where('status', '=', '1')
                                ->where('isdeleted', '=', '0')
                                ->orderby('id','DESC')
                                ->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Business location lists','error'=>'','success'=>$Businesslocation], 200);                     
          } else {
          return response()->json(['status'=>false,'message'=>'Business location not found','error'=>'','data'=>''], 200);
          }
        } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 200);
        }  
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }
    
  public function getBlDetail(Request $request, $id) {
    try{
        $isBusinesslocation = Businesslocations::where('id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        if($isBusinesslocation != 0) {
          $Businesslocation = Businesslocations::where('id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
          return response()->json(['status'=>true,'message'=>'Business location detail','error'=>'','data'=>$Businesslocation], 200);
        } else {
          return response()->json(['status'=>false,'message'=>'Business location not found','error'=>'','data'=>''], 200);
        }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateBl(Request $request, $id) {
    try{
        $Input =  [];
        $businessName = urlencode(trim($request->find_business_location));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
      if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {
          $business_page_id = $data['candidates'][0]['place_id'];
          $business_name = trim($request->business_name); // $data['candidates'][0]['name'];
          $chkBusiness = Businesslocations::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($chkBusiness == 0) {
            $business_review_link = trim($request->business_review_link);
            $chkBrl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
            if($chkBrl == 0) {
                $Input['find_business_location'] = trim($request->find_business_location);
                $Input['business_page_id'] = $data['candidates'][0]['place_id'];
                $Input['business_address'] = $data['candidates'][0]['formatted_address'];
                $Input['business_name'] = trim($request->business_name);
                // $data['candidates'][0]['name'];
                // $Input['business_rating'] = $data['candidates'][0]['rating'];
                $Input['lat'] = trim($request->lat);
                $Input['lng'] = trim($request->lng);
                $Input['phone'] = trim($request->phone);
                $Input['business_rating'] = '';
                $Input['business_logo'] = '';
                $Input['business_review_link'] = trim($request->business_review_link);

                if(trim($request->facebook_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->facebook_link))) {
                      $urlFb = "https://" . trim($request->facebook_link);
                      $Input['facebook_link'] = $urlFb;
                    } else {
                      $Input['facebook_link'] = trim($request->facebook_link);
                    }
                }

                if(trim($request->twitter_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->twitter_link))) {
                      $urlTwt = "https://" . trim($request->twitter_link);
                      $Input['twitter_link'] = $urlTwt;
                    } else {
                      $Input['twitter_link'] = trim($request->twitter_link);
                    }
                }

                if(trim($request->linkedin_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->linkedin_link))) {
                      $urlLnkdin = "https://" . trim($request->linkedin_link);
                      $Input['linkedin_link'] = $urlLnkdin;
                    } else {
                      $Input['linkedin_link'] = trim($request->linkedin_link);
                    }
                }
                
                if(trim($request->instagram_link)!='') {
                  if (!preg_match("~^(?:f|ht)tps?://~i", trim($request->instagram_link))) {
                      $urlInsta = "https://" . trim($request->instagram_link);
                      $Input['instagram_link'] = $urlInsta;
                    } else {
                      $Input['instagram_link'] = trim($request->instagram_link);
                    }
                }
                
                /*$Input['facebook_link'] = trim($request->facebook_link);
                $Input['twitter_link'] = trim($request->twitter_link);
                $Input['linkedin_link'] = trim($request->linkedin_link);
                $Input['instagram_link'] = trim($request->instagram_link);*/

                $Input['client_satisfaction'] = trim($request->client_satisfaction);
                $Input['updated_by'] = $id;
                Businesslocations::where('id', $id)->update($Input);
                $Businesslocation = Businesslocations::where('id', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();

                $chkLrv = Livereviews::where('business_place_id', '=', $Businesslocation->business_page_id)->count();
                if($chkLrv == 0) {

                  $TotalRatings = 0;
                  $per_page = '10';
                  $business_page_id = $Businesslocation->business_page_id;
                  $user_id = $Businesslocation->user_id;
                  $business_review_link = $Businesslocation->business_review_link;
                  $myblid = $Businesslocation->id;

                  $urlTop5 = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
                      $jsonTop5 = file_get_contents($urlTop5);
                      $dataTop5 = json_decode($jsonTop5, true);
                 
                  if($dataTop5['status']=='OK') { 
                    if(array_key_exists("user_ratings_total", $dataTop5['result'])){
                      $TotalRatingsCount = $dataTop5['result']['user_ratings_total'];
                      if( $TotalRatingsCount > 200 ) {
                        $TotalRatings = '200';
                      } else {
                        $TotalRatings = $dataTop5['result']['user_ratings_total'];
                      }
                      $totalPage = ceil( $TotalRatings / $per_page);
                      for($index=$totalPage;$index > 0;$index-=1) {
                        $from = ($index - 1) * 10;
                        $this->saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid);
                      }
                    } 
                  }

                }

                return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$Businesslocation], 200);
            } else {
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200); 
            }
          } else {
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>''], 200);
          }
        } else {
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>''], 200);
        }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }

public function getAutofillDetails(Request $request) {
    try{
      $isBn = trim($request->find_business_location);
      if ($isBn!='') {
        $Input =  [];
        $businessName = urlencode(trim($request->find_business_location));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
        if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {
          $Input['business_page_id'] = $data['candidates'][0]['place_id'];
          $Input['business_name'] = $data['candidates'][0]['name'];
          $Input['business_address'] = $data['candidates'][0]['formatted_address'];
          return response()->json(['status'=>true,'message'=>'Auto fill detail','error'=>'','data'=>$Input], 200);
        } else {
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>''], 200);
        }
      } else {
        return response()->json(['status'=>false,'message'=>'Bn not found.','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }  

public function getFilterBl(Request $request, $id) {
    try{
        $isUser = User::find($id);
        if ($isUser) {
          $isBusinesslocation = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
          if($isBusinesslocation != 0) {
            $Businesslocation = Businesslocations::where('user_id', '=', $id)
                                ->where('status', '=', '1')
                                ->where('isdeleted', '=', '0')
                                ->orderby('id','DESC')
                                ->get();
            return response()->json(['status'=>true,'message'=>'Business location lists','error'=>'','data'=>$Businesslocation], 200);                     
          } else {
          return response()->json(['status'=>false,'message'=>'Business location not found','error'=>'','data'=>''], 200);
          }
        } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 200);
        }  
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getSingleBlDetail(Request $request, $id) {
    try{
        $isBusinesslocation = Businesslocations::where('id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        if($isBusinesslocation != 0) {
          $Businesslocation = Businesslocations::where('id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first()->toArray();
          return response()->json(['status'=>true,'message'=>'Business location detail','error'=>'','data'=>$Businesslocation], 200);
        } else {
          return response()->json(['status'=>false,'message'=>'Business location not found','error'=>'','data'=>''], 200);
        }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }   

public function guard()
{
  return Auth::guard();
}  

}
