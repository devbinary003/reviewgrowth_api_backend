<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

use App\Models\User;
use App\Models\Loginhistory;
use App\Http\Resources\UserResource;

use DB;
use Mail;
use Log;
use Image;
use URL;
use JWTAuth;
use JWTFactory;
use Carbon\Carbon; 

use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;

class LoginhistoryController extends Controller
{   

    public function allusersloginLists(Request $request)
    {
        try{
            $loggedins = Loginhistory::where('status', '=', '1')->where('deleted', '=', '0')->orderby('id','DESC')->paginate(Config::get('constant.pagination'));
            return response()->json(['status'=>true,'message'=>'Loggedins detail','error'=>'','data'=>$loggedins], 200);
         } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
    }

    public function singleuserloginList(Request $request, $id) {
       try{
            $loggedins = Loginhistory::where('status', '=', '1')
                    ->where('deleted', '=', '0')
                    ->where('user_id', '=', $id)
                    ->orderBy('id','DESC')
                    ->paginate(Config::get('constant.pagination'));
           
            return response()->json(['status'=>true,'message'=>'Loggedins detail','error'=>'','data'=>$loggedins], 200);
         } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
    }

    public function loginSearch(Request $request) {
       try{
            $input = $request->all();
            $search_data = $input['keyword'];
            $loggedins = Loginhistory::where('status', '=', '1')
                    ->where('deleted', '=', '0')
                    ->where('user_id', '=', $search_data)
                    ->orderBy('id','DESC')
                    ->paginate(Config::get('constant.pagination'));
           
            return response()->json(['status'=>true,'message'=>'Loggedins detail','error'=>'','data'=>$loggedins], 200);
         } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }
    }

    public function getLoginDetail(Request $request, $id) {
      try{
        $isLoggedin = Loginhistory::find($id);
        if ($isLoggedin) {
            $loggedin = Loginhistory::where('id', $id)->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Loggedin detail','error'=>'','data'=>$loggedin], 200);
            } else {
            return response()->json(['status'=>false,'message'=>'Loggedin not found','error'=>'','data'=>''], 400);
            }
         } catch(\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
        }

    }

    
}