<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Businessreview;
use App\Models\Businesslocations;
use App\Models\Livereviews;
use Illuminate\Support\Facades\Auth;
use App\Mail\SendMailSatisfiedReview;
use App\Mail\SendMailUnSatisfiedReview;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon; 
use JWTFactory;
use JWTAuth;
use Validator;
use Config;
use Log;
use Event;
use Mail;
use App\Events\UserRegistered;
use DB;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Classes\ErrorsClass;
use Image;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class BusinessreviewController extends Controller
{  
  
  public function getInternalreviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;

          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getInternalreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;

          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getSatisfiedreviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '>=', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Satisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getSatisfiedreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '>=', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Satisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }
  
  public function getUnsatisfiedreviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '<', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Unsatisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getUnsatisfiedreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '<', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Unsatisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }  

  public function getLivereviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          
          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All live reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getLivereviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;

          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All live reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }
  
  public function getLvSatisfiedreviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('live_review_rating', '>=', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Live satisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }

  public function getLvSatisfiedreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('live_review_rating', '>=', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Live satisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }
  
  public function getLvUnsatisfiedreviewsDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('live_review_rating', '<', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Live unsatisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }

  public function getLvUnsatisfiedreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('live_review_rating', '<', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'Live unsatisfied reviews listing','error'=>'','success'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }

  public function getLiveRating(Request $request, $id) {
    try{
        $Businesslocation = Businesslocations::where('id', $id)->first();
        $businessName = urlencode(trim($Businesslocation->business_name));
        $business_page_id = $Businesslocation->business_page_id;
        $url = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
          if($data['status']=='OK') {
            if(array_key_exists("rating", $data['result'])){
                      $rating = $data['result']['rating'];
              return response()->json(['status'=>true,'message'=>'Live rating','error'=>'','data'=>$rating], 200);          
            } else {
              return response()->json(['status'=>false,'message'=>'No rating found.','error'=>'','data'=>''], 200);
            }
          } else {
              return response()->json(['status'=>false,'message'=>'No rating found.','error'=>'','data'=>''], 200);
          }
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
  }

  /*
  public function getLiveRating(Request $request, $id) {
    try{
        $Businesslocation = Businesslocations::where('id', $id)->first();
        $businessName = urlencode(trim($Businesslocation->business_name));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);
          if($data['status']=='OK') {
            $rating = $data['candidates'][0]['rating'];
            if( $rating!='' ) {
              return response()->json(['status'=>true,'message'=>'Live rating','error'=>'','data'=>$rating], 200);
            } else {
              return response()->json(['status'=>false,'message'=>'No rating found.','error'=>'','data'=>''], 200);
            }
          } else {
              return response()->json(['status'=>false,'message'=>'No rating found.','error'=>'','data'=>''], 200);
          }

      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }
  */

  public function getTopLiveReviews(Request $request, $id) {
    try{

        $Businesslocation = Businesslocations::where('id', $id)->first();
        $business_page_id = $Businesslocation->business_page_id;
        $business_review_link = $Businesslocation->business_review_link;
        $user_id = $Businesslocation->user_id;
        $client_satisfaction = $Businesslocation->client_satisfaction;
        $myblid = $Businesslocation->id;

        /*$countLvrvw = Livereviews::where('business_user_id', '=', $user_id)
                      ->where('business_place_id', '=', $business_page_id)
                      ->where('business_slug', '=', $business_review_link)
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->count();
        if($countLvrvw == 0) {
          
          $TotalRatings = 0;
          $per_page = '10';

        $urlTop5 = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
            $jsonTop5 = file_get_contents($urlTop5);
            $dataTop5 = json_decode($jsonTop5, true); 
          if($dataTop5['status']=='OK') { 
              if(array_key_exists("user_ratings_total", $dataTop5['result'])){ 
                  $TotalRatings = $dataTop5['result']['user_ratings_total'];
                  $totalPage = ceil( $TotalRatings / $per_page);
                for($index=$totalPage;$index > 0;$index-=1) {
                  $from = ($index - 1) * 10;
                $this->saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid);
                }  
              }
            } 

        }*/ 

        $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                      ->where('business_place_id', '=', $business_page_id)
                      ->where('business_slug', '=', $business_review_link)
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->limit(5)
                      ->get();
        return response()->json(['status'=>true,'message'=>'Top five live reviews','error'=>'','data'=>$reviewlist], 200);

      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

public function getMylivereviews(Request $request, $id) {
    try{

        $Businesslocation = Businesslocations::where('id', $id)->first();
        $business_page_id = $Businesslocation->business_page_id;
        $business_review_link = $Businesslocation->business_review_link;
        $user_id = $Businesslocation->user_id;
        $client_satisfaction = $Businesslocation->client_satisfaction;
        $myblid = $Businesslocation->id;

        $countLvrvw = Livereviews::where('business_user_id', '=', $user_id)
                      ->where('business_place_id', '=', $business_page_id)
                      ->where('business_slug', '=', $business_review_link)
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->count();
        if($countLvrvw == 0) {
          
          $TotalRatings = 0;
          $per_page = '10';

        $urlTop5 = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
            $jsonTop5 = file_get_contents($urlTop5);
            $dataTop5 = json_decode($jsonTop5, true); 
          if($dataTop5['status']=='OK') { 
              if(array_key_exists("user_ratings_total", $dataTop5['result'])){ 
                  $TotalRatingsCount = $dataTop5['result']['user_ratings_total'];
                  if( $TotalRatingsCount > 200 ) {
                    $TotalRatings = '200';
                  } else {
                    $TotalRatings = $dataTop5['result']['user_ratings_total'];
                  }
                  $totalPage = ceil( $TotalRatings / $per_page);
                for($index=$totalPage;$index > 0;$index-=1) {
                  $from = ($index - 1) * 10;
                $this->saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid);
                }  
              }
            } 

        } else {  

        $LivReviews = Livereviews::where('business_user_id', '=', $user_id)
                      ->where('business_place_id', '=', $business_page_id)
                      ->where('business_slug', '=', $business_review_link)
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->limit(10)
                      ->pluck('live_review_reviewer')
                      ->toArray();
        $from = '0';
        $lvUrl = "https://wextractor.com/api/v1/reviews?offset=".$from."&id=".$business_page_id."&auth_token=".env('WEX_AUTH_TOKEN');
        $lvJson = file_get_contents($lvUrl);
        $lvData = json_decode($lvJson, true);
        if( count($lvData['reviews']) > 0 ) {
          $lvreviews = array_reverse($lvData['reviews']);
          foreach($lvreviews as $lvreview) {
            if (in_array($lvreview['reviewer'], $LivReviews)) {
            // echo "Value exists : ".$lvreview['id'].'^^^^^^'.$lvreview['reviewer'].'<br>'; 
              } else {
              if($lvreview['text']!='') {
                    $live_review_text = $lvreview['text'];
                } else {
                    $live_review_text = 'No review was written for this submission.';
                }
                DB::table('live_reviews')->insert(
                  ['live_review_id' => $lvreview['id'], 'live_review_reviewer' => $lvreview['reviewer'], 'live_review_datetime' => $lvreview['datetime'], 'live_review_rating' => $lvreview['rating'], 'live_review_text' => $live_review_text, 'live_review_url' => $lvreview['url'], 'live_review_language' => $lvreview['language'], 'business_user_id' => $user_id, 'business_place_id' => $business_page_id, 'business_slug' => $business_review_link]
                );
              }
          }
        }
      }
        
        $reviewlist = Livereviews::where('business_user_id', '=', $user_id)
                      ->where('business_place_id', '=', $business_page_id)
                      ->where('business_slug', '=', $business_review_link)
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->limit(5)
                      ->get();
        return response()->json(['status'=>true,'message'=>'Top five live reviews','error'=>'','data'=>$reviewlist], 200);

      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

public function saveLiveReviews($from,$user_id,$business_page_id,$business_review_link,$myblid) {
    try{
         $lvUrl = "https://wextractor.com/api/v1/reviews?offset=".$from."&id=".$business_page_id."&auth_token=".env('WEX_AUTH_TOKEN');
          $lvJson = file_get_contents($lvUrl);
          $lvData = json_decode($lvJson, true);
          if( count($lvData['reviews']) > 0 ) {
            $lvreviews = array_reverse($lvData['reviews']);
            foreach($lvreviews as $lvreview) {
              if($lvreview['text']!='') {
                $live_review_text = $lvreview['text'];
              } else {
                $live_review_text = 'No review was written for this submission.';
              }
              DB::table('live_reviews')->insert(
              ['live_review_id' => $lvreview['id'], 'live_review_reviewer' => $lvreview['reviewer'], 'live_review_datetime' => $lvreview['datetime'], 'live_review_rating' => $lvreview['rating'], 'live_review_text' => $live_review_text, 'live_review_url' => $lvreview['url'], 'live_review_language' => $lvreview['language'], 'business_user_id' => $user_id, 'business_place_id' => $business_page_id, 'business_slug' => $business_review_link]
             );
            }
            DB::table('businesslocations')
                ->where('id', $myblid)
                ->update(['lrvstatus' => '1']);
          }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }
  
  public function getMyinternalreviews(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->limit(5)
                        ->get();
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    } 

  public function getAll(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->limit(5)
                        ->get();
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    } 

  /*public function getSatisfied(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '>=', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->limit(5)
                        ->get();
          return response()->json(['status'=>true,'message'=>'Satisfied reviews listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getUnsatisfied(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          if($client_satisfaction == 0){
             $clientsecondvalue = '5';
          } else {
             $clientsecondvalue = '4';
          }
          $reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where('reviewer_rating', '<', $clientsecondvalue)
                        ->where('status', '=', '1')
                        ->where('is_deleted', '=', '0')
                        ->orderby('id','DESC')
                        ->limit(5)
                        ->get();
          return response()->json(['status'=>true,'message'=>'Unsatisfied reviews listing','error'=>'','data'=>$reviewlist], 200);
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }*/  

  public function getSearchReviewDef(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          $search_data = trim($request->searchKey);

          $reviewlistOffline = Businessreview::select('reviewer_name as ruv_name', 'reviewer_rating as ruv_rating', 'reviewer_description as ruv_desc', 'created_at as ruv_datetime')
                        ->where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('reviewer_name', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_email', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_description', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->get();

          $reviewlistLive = Livereviews::select('live_review_reviewer as ruv_name', 'live_review_rating as ruv_rating', 'live_review_text as ruv_desc', 'live_review_datetime as ruv_datetime')
                        ->where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('live_review_reviewer', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_datetime', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_text', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->get();

         // $all = $reviewlistLive->merge($reviewlistOffline); 
         // $arr = $all->toArray();

          $arr = array_merge($reviewlistLive->toArray(), $reviewlistOffline->toArray()); 
          $perPage = 10;
          $input = \Input::all();
          if (isset($input['page']) && !empty($input['page'])) { 
            $currentPage = $input['page']; 
          } else { 
            $currentPage = 1; 
          }
          
          $offset = $perPage * ($currentPage - 1); // ($currentPage * $perPage) - $perPage;
          $arr_splice = array_slice($arr, $offset, $perPage, true);
          $paginator = new Paginator($arr_splice, count($arr), $perPage, $currentPage);

          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$paginator,'searchKey'=>$search_data], 200);

          /*$reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('reviewer_name', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_email', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_description', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$reviewlist,'searchKey'=>$search_data], 200);*/
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }

  public function getSearchReview(Request $request, $id) {
    try{
          $Businesslocation = Businesslocations::where('id', $id)->first();
          $business_page_id = $Businesslocation->business_page_id;
          $business_review_link = $Businesslocation->business_review_link;
          $user_id = $Businesslocation->user_id;
          $client_satisfaction = $Businesslocation->client_satisfaction;
          $search_data = trim($request->searchKey);

          $reviewlistOffline = Businessreview::select('reviewer_name as ruv_name', 'reviewer_rating as ruv_rating', 'reviewer_description as ruv_desc', 'created_at as ruv_datetime')
                        ->where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('reviewer_name', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_email', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_description', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->get();

          $reviewlistLive = Livereviews::select('live_review_reviewer as ruv_name', 'live_review_rating as ruv_rating', 'live_review_text as ruv_desc', 'live_review_datetime as ruv_datetime')
                        ->where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('live_review_reviewer', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_datetime', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('live_review_text', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->get();

         // $all = $reviewlistLive->merge($reviewlistOffline); 
         // $arr = $all->toArray();
                      
         $arr = array_merge($reviewlistLive->toArray(), $reviewlistOffline->toArray()); 
          $perPage = 10;
          $input = \Input::all();
          if (isset($input['page']) && !empty($input['page'])) { 
            $currentPage = $input['page']; 
          } else { 
            $currentPage = 1; 
          }
          
          $offset = $perPage * ($currentPage - 1); // ($currentPage * $perPage) - $perPage;
          $arr_splice = array_slice($arr, $offset, $perPage, true);
          $paginator = new Paginator($arr_splice, count($arr), $perPage, $currentPage);

          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$paginator,'searchKey'=>$search_data], 200);

          /*$reviewlist = Businessreview::where('business_user_id', '=', $user_id)
                        ->where('business_place_id', '=', $business_page_id)
                        ->where('business_slug', '=', $business_review_link)
                        ->where(
                      function($query) use ($search_data){
                        $query->where('reviewer_name', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_email', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_rating', 'LIKE', '%'.$search_data.'%');
                        $query->orWhere('reviewer_description', 'LIKE', '%'.$search_data.'%');
                      })
                      ->where('status', '=', '1')
                      ->where('is_deleted', '=', '0')
                      ->orderby('id','DESC')
                      ->paginate(Config::get('constant.pagination'));
          return response()->json(['status'=>true,'message'=>'All reviews listing','error'=>'','success'=>$reviewlist,'searchKey'=>$search_data], 200);*/
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
      } catch(\Exception $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
        return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
      }
    }
  

}