<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;
use App\Mail\UserApprovalMail;
use App\Mail\UserDisapprovalMail;
use App\Mail\UnarchiveUserMail;
use App\Models\User;
use App\Models\Businessreview;
use App\Models\Businesslocations;
use App\Models\Livereviews;
use App\Http\Resources\UserResource;

use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use DB;
use Mail;
use Log;
use Image;
use URL;
use JWTAuth;
use JWTFactory;
use Carbon\Carbon; 

use Illuminate\Support\Str;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Input;
use App\Classes\ErrorsClass;
//use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{ 

  public function getuserDetails(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $user = User::where('id', $id)->first()->toArray();
        $locationCount = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        return response()->json(['status'=>true,'message'=>'User details','error'=>'','data'=>$user,'locationCount'=>$locationCount], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateFindbusiness(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $Input =  [];
        $businessName = urlencode(trim($request->business_name));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);

        if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {

          $business_page_id = $data['candidates'][0]['place_id'];
          $business_name = $data['candidates'][0]['name'];
          
          $chkBusiness = User::where('business_page_id', '=', $business_page_id)->where('business_name', '=', $business_name)->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();

          if($chkBusiness == 0) {

              $Input['business_page_id'] = $data['candidates'][0]['place_id'];
              $Input['business_address'] = $data['candidates'][0]['formatted_address'];
              $Input['business_name'] = $data['candidates'][0]['name'];
            //  $Input['business_rating'] = $data['candidates'][0]['rating'];
              
              $Input['lat'] = trim($request->lat);
              $Input['long'] = trim($request->lng);
              $Input['reg_step_1'] = '1';
              $Input['updated_by'] = $id;
              $Input['updated_at'] = date('Y-m-d h:i:s');

              User::where('id', $id)->update($Input);

            $user = User::where('id', $id)->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$user], 200);

            } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>$user], 200);
            }

        } else {
          $user = User::where('id', $id)->first()->toArray();
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>$user], 200);
        }
        
      } else {
        return response()->json(['status'=>false,'message'=>'User not found.','error'=>'','data'=>''], 400);
      }

    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateBusinessinfo(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $Input =  [];
        $businessName = urlencode(trim($request->business_name));
        $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
        $json = file_get_contents($url);
        $data = json_decode($json, true);

        if($data['status']=='OK' && $data['candidates'][0]['place_id'] !='' && $data['candidates'][0]['name'] !='' && $data['candidates'][0]['formatted_address'] !='' ) {

          $business_page_id = $data['candidates'][0]['place_id'];
          $business_name = trim($request->business_name); // $data['candidates'][0]['name'];
          
          $chkBusiness = User::where('business_name', '=', $business_name)->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();

          if($chkBusiness == 0) {

            $chkUser = User::where('email', '=', trim($request->email))->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();

            if ( $chkUser == 0 ) {

              $Input['business_page_id'] = $data['candidates'][0]['place_id'];
              $Input['business_address'] = $data['candidates'][0]['formatted_address'];
              $Input['business_name'] = trim($request->business_name); //$data['candidates'][0]['name'];
            //  $Input['business_rating'] = $data['candidates'][0]['rating'];
              if(trim($request->lat) !='' && trim($request->lng) !='') {
                $Input['lat'] = trim($request->lat);
                $Input['long'] = trim($request->lng);
              }

              $Input['firstname'] = ucfirst(trim($request->firstname));
              $Input['lastname'] = ucfirst(trim($request->lastname));
              $Input['fullname'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));

              $Input['email'] = trim($request->email);
              $Input['phone'] = trim($request->phone);

              $Input['name_on_card'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
              
              $number_of_locations = trim($request->number_of_locations);

              if ($number_of_locations != '') {

                if($number_of_locations == 1) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_FnmzVv6VFQIgWq';
                } elseif($number_of_locations == 2) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn01drULw2c7t';
                } elseif($number_of_locations == 3) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn0ZbdDhZOAVL';  
                } elseif($number_of_locations == 4) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn1HhexTiEK7E';
                } elseif($number_of_locations == 5) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn2EDDErBCSLC';
                } elseif($number_of_locations == 6) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn30KnfkLDjma';
                } elseif($number_of_locations == 7) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn4mSV3mCePvu';
                } elseif($number_of_locations == 8) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn4ezUcgKXvV2';
                } elseif($number_of_locations == 9) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn59W1wSSckf6';
                } elseif($number_of_locations == 10) {
                  $item_qnty = $number_of_locations - 1;
                  $total_sum = 47 + ($item_qnty * 10);
                  $planId = 'plan_Fnn6gaNt82a4o5';
                } else {
                  $item_qnty = '';
                  $total_sum = '';
                  $planId = '';
                }

                $Input['number_of_locations'] = trim($request->number_of_locations);
                $Input['item_qnty'] = $item_qnty;
                $Input['total_sum'] = $total_sum;
                $Input['plan_id'] = $planId;
              }

              $Input['reg_step_2'] = '1';
              $Input['updated_by'] = $id;
              $Input['updated_at'] = date('Y-m-d h:i:s');

            User::where('id', $id)->update($Input);
            $user = User::where('id', $id)->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$user], 200);

            } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'Business email is already in use.','error'=>'','data'=>$user], 200);
            }

            } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'This business name is already in use.','error'=>'','data'=>$user], 200);
            }

        } else {
          $user = User::where('id', $id)->first()->toArray();
          return response()->json(['status'=>false,'message'=>'Either google place id or business name could not be found for the entered business name, so please choose a valid business name.','error'=>'','data'=>$user], 200);
        }

      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }

    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateCustomizereview(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $Input =  [];
        $business_review_link = trim($request->business_review_link);
        $chkBrl = User::where('business_review_link', '=', $business_review_link)->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();

        if($chkBrl == 0) {

          $Input['business_review_link'] = trim($request->business_review_link);
          $Input['facebook_link'] = trim($request->facebook_link);
          $Input['twitter_link'] = trim($request->twitter_link);
          $Input['linkedin_link'] = trim($request->linkedin_link);
          $Input['instagram_link'] = trim($request->instagram_link);
          $Input['client_satisfaction'] = trim($request->client_satisfaction);

          $Input['sc_emsg'] = 'Thank you for your kind words, and filling out our in office feedback survey. Help us out by makng your review go live.';
          $Input['sc_ebtnclr'] = '#333';
          $Input['sc_enote'] = 'Thanks Again for your kind words.';
          $Input['unsc_emsg'] = 'Thnak you so much for your honest feedback. So sorry to here you were not 100% satisfied at our business.';
          
          $Input['reg_step_3'] = '1';
          $Input['updated_by'] = $id;
          $Input['updated_at'] = date('Y-m-d h:i:s');  

        User::where('id', $id)->update($Input);
        $user = User::where('id', $id)->first()->toArray();
        return response()->json(['status'=>true,'message'=>'Data saved successfully','error'=>'','data'=>$user], 200);

        } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>$user], 200);
            }
        
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }

    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateBilling(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
          $user  = User::find($id);
          Stripe::setApiKey('sk_test_jQC9RMvcbVHZ7Q3cn395zWLA'); 
          $token = trim($request->forwardToken);
          $number_of_locations = trim($request->number_of_locations);

        if ($token != '' && $number_of_locations != '') {
          
          if($user->customer_id != '') {

            $chkCustomer = \Stripe\Customer::retrieve($user->customer_id);
            if($user->customer_id == $chkCustomer['id']) {
            $customerId = $chkCustomer['id'];
            }  else {
              $customer = \Stripe\Customer::create(array(
                            "description" => $user->fullname,
                            "email" => $user->email,
                            "source" => $token,
                            "metadata" => array("address_line1" => $user->fullname, "address_city" => $user->business_name, "address_state" => $user->business_address, "address_zip" => $user->business_page_id, "address_country" => $user->phone)
                          ));
              $customerId = $customer['id'];
            }
          } else {

            $customer = \Stripe\Customer::create(array(
                            "description" => $user->fullname,
                            "email" => $user->email,
                            "source" => $token,
                            "metadata" => array("address_line1" => $user->fullname, "address_city" => $user->business_name, "address_state" => $user->business_address, "address_zip" => $user->business_page_id, "address_country" => $user->phone)
                          ));
            $customerId = $customer['id'];
          }

          $subscription = \Stripe\Subscription::create(array(
                              "customer" => $customerId,
                                "items" => array(
                                      array(
                                      "plan" => $user->plan_id,
                                  ),
                                ),
                            ));


        if($subscription['status'] == 'active') {

        $Input =  [];

        $Input['customer_id'] = trim($subscription['customer']);
        $Input['subscription_id'] = trim($subscription['id']);
        $Input['plan_id'] = trim($subscription['plan']['id']);
        $Input['amount'] = trim($subscription['plan']['amount']);
        $Input['subscription_status'] = trim($subscription['status']);
        $Input['current_period_start'] = trim($subscription['current_period_start']);
        $Input['current_period_end'] = trim($subscription['current_period_end']);
        $Input['start_date'] = trim($subscription['start_date']);
       
        $Input['card_number'] = trim($request->card_number);
        $Input['card_exp_month'] = trim($request->card_exp_month);
        $Input['exp_year'] = trim($request->exp_year);
        $Input['name_on_card'] = trim($request->name_on_card);

        $Input['reg_step_4'] = '1';
        $Input['isprofilecomplete'] = '1';
        $Input['updated_by'] = $id;
        $Input['updated_at'] = date('Y-m-d h:i:s');

        User::where('id', $id)->update($Input);
        $user = User::where('id', $id)->first()->toArray();

       /* $TotalRatings = 0;
        $per_page = '10';

        $Businesslocation = Businesslocations::where('user_id', '=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();
        $business_page_id = $Businesslocation->business_page_id;
        $user_id = $Businesslocation->user_id;
        $business_review_link = $Businesslocation->business_review_link;

        $urlTop5 = "https://maps.googleapis.com/maps/api/place/details/json?place_id=".$business_page_id."&key=".env('GOOGLE_API_KEY');
            $jsonTop5 = file_get_contents($urlTop5);
            $dataTop5 = json_decode($jsonTop5, true);
       
          if($dataTop5['status']=='OK') { 
            if(array_key_exists("user_ratings_total", $dataTop5['result'])){ 
                $TotalRatings = $dataTop5['result']['user_ratings_total'];
                $totalPage = ceil( $TotalRatings / $per_page);
              for($index=$totalPage;$index > 0;$index-=1) {
                $from = ($index - 1) * 10;
              $this->saveLiveReviews($from,$user_id,$business_page_id,$business_review_link);
              }  
            }
          } */

        return response()->json(['status'=>true,'message'=>'Data saved successfully','error'=>'','data'=>$user], 200);

        } else {
          $user = User::where('id', $id)->first()->toArray();
          return response()->json(['status'=>false,'message'=>'Sorry fail to subscribe','error'=>'','data'=>$user], 421);
        }

        } else {
          $user = User::where('id', $id)->first()->toArray();
          return response()->json(['status'=>false,'message'=>'Sorry fail to subscribe','error'=>'','data'=>$user], 421);
           }

      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 421);
      }

    } catch(\Stripe\Error\Card $e) {
     $body = $e->getJsonBody();
     $err  = $body['error'];
     return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 422);
} catch (\Stripe\Error\RateLimit $e) {
    $body = $e->getJsonBody();
    $err  = $body['error'];
    return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 423);
} catch (\Stripe\Error\InvalidRequest $e) {
    $body = $e->getJsonBody();
    $err  = $body['error'];
    return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 424);
} catch (\Stripe\Error\Authentication $e) {
    $body = $e->getJsonBody();
    $err  = $body['error'];
    return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 425);
} catch (\Stripe\Error\ApiConnection $e) {
    $body = $e->getJsonBody();
    $err  = $body['error'];
    return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 426);
} catch (\Stripe\Error\Base $e) {
    $body = $e->getJsonBody();
    $err  = $body['error'];
    return response()->json(['status'=>false,'message'=>$err['message'],'error'=>$err['message'],'data'=>''], 427);
}  catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

}

public function saveLiveReviews($from,$user_id,$business_page_id,$business_review_link) {
    try{
         $lvUrl = "https://wextractor.com/api/v1/reviews?offset=".$from."&id=".$business_page_id."&auth_token=".env('WEX_AUTH_TOKEN');
          $lvJson = file_get_contents($lvUrl);
          $lvData = json_decode($lvJson, true);
          if( count($lvData['reviews']) > 0 ) {
            $lvreviews = array_reverse($lvData['reviews']);
            foreach($lvreviews as $lvreview) {
              if($lvreview['text']!='') {
                $live_review_text = $lvreview['text'];
              } else {
                $live_review_text = 'No review was written for this submission.';
              }
              DB::table('live_reviews')->insert(
              ['live_review_id' => $lvreview['id'], 'live_review_reviewer' => $lvreview['reviewer'], 'live_review_datetime' => $lvreview['datetime'], 'live_review_rating' => $lvreview['rating'], 'live_review_text' => $live_review_text, 'live_review_url' => $lvreview['url'], 'live_review_language' => $lvreview['language'], 'business_user_id' => $user_id, 'business_place_id' => $business_page_id, 'business_slug' => $business_review_link]
             );
            }
          }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateAccountsettings(Request $request, $id) {
    try{
      $isUser = User::find($id);
      if ($isUser) {
        $Input =  [];
          $chkUser = User::where('email', '=', trim($request->email))->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();
            if ( $chkUser == 0 ) {
              $Input['firstname'] = ucfirst(trim($request->firstname));
              $Input['lastname'] = ucfirst(trim($request->lastname));
              $Input['fullname'] = ucfirst(trim($request->firstname)).' '.ucfirst(trim($request->lastname));
              $Input['phone'] = trim($request->phone);
              $Input['address_line_1'] = ucfirst(trim($request->address_line_1));
              $Input['address_line_2'] = ucfirst(trim($request->address_line_2));
              $Input['city'] = ucfirst(trim($request->city));
              $Input['state'] = ucfirst(trim($request->state));
              $Input['country'] = ucfirst(trim($request->country));
              $Input['zipcode'] = trim($request->zipcode);
              $Input['updated_by'] = $id;
              $Input['updated_at'] = date('Y-m-d h:i:s');

            User::where('id', $id)->update($Input);
            $user = User::where('id', $id)->first()->toArray();
            return response()->json(['status'=>true,'message'=>'Data saved successfully.','error'=>'','data'=>$user], 200);
            } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'Email is already in use.','error'=>'','data'=>$user], 200);
            }
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }

    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

  public function updateSystemsettings(Request $request, $id) {

    try{
      $isUser = User::find($id);
      if ($isUser) {
        $Input =  [];
        $business_review_link = trim($request->business_review_link);
        $chkBrl = User::where('business_review_link', '=', $business_review_link)->where('id', '!=', $id)->where('isdeleted', '=', '0')->count();
        
        if($chkBrl == 0) {
          $Input['business_review_link'] = trim($request->business_review_link);
          $Input['facebook_link'] = trim($request->facebook_link);
          $Input['twitter_link'] = trim($request->twitter_link);
          $Input['linkedin_link'] = trim($request->linkedin_link);
          $Input['instagram_link'] = trim($request->instagram_link);
          $Input['client_satisfaction'] = trim($request->client_satisfaction);
          $Input['updated_by'] = $id;
          $Input['updated_at'] = date('Y-m-d h:i:s');

        User::where('id', $id)->update($Input);
        $user = User::where('id', $id)->first()->toArray();
        return response()->json(['status'=>true,'message'=>'Data saved successfully','error'=>'','data'=>$user], 200);

          } else {
              $user = User::where('id', $id)->first()->toArray();
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>$user], 200);
            }
        
      } else {
        return response()->json(['status'=>false,'message'=>'User not found','error'=>'','data'=>''], 400);
      }

    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }

public function updateReviewlink(Request $request, $id) {

    try{
        $Input =  [];
        $business_review_link = trim($request->business_review_link);
        $chkBrl = Businesslocations::where('business_review_link', '=', $business_review_link)->where('id', '!=', $id)->where('status', '=', '1')->where('isdeleted', '=', '0')->count();
        if($chkBrl == 0) {
          $Input['business_review_link'] = trim($request->business_review_link);
          $Input['updated_by'] = $id;
          $Input['updated_at'] = date('Y-m-d h:i:s');
        Businesslocations::where('id', $id)->update($Input);
        return response()->json(['status'=>true,'message'=>'Your business review link updated successfully','error'=>'','data'=>''], 200);
          } else {
              return response()->json(['status'=>false,'message'=>'This business review link is already in use.','error'=>'','data'=>''], 200);
          }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }

  }
  

  public function checkemail(Request $request) {
    try{
      $user = User::where('email', '=', trim($request->email))->where('isdeleted', '=', '0')->count();
      if ( $user > 0 ) {
        return response()->json(['status'=>false,'message'=>'Email already exists','error'=>'','data'=>''], 401);
      } else {
        return response()->json(['status'=>true,'message'=>'Email avaliable','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }

  public function checkusername(Request $request) {
    try{
      $user = User::where('username', '=', trim($request->username))->where('isdeleted', '=', '0')->count();
      if ( $user > 0 ) {
        return response()->json(['status'=>false,'message'=>'Username already exists','error'=>'','data'=>''], 401);
      } else {
        return response()->json(['status'=>true,'message'=>'Username avaliable','error'=>'','data'=>''], 200);
      }
    } catch(\Illuminate\Database\QueryException $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
    } catch(\Exception $e) {
      $errorClass = new ErrorsClass();
      $errors = $errorClass->saveErrors($e);
      return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
    }
  }

public function checkuserlogin(Request $request) {
   try{
    $userid = $request->userid;
    $users = User::where('id', $userid)->where('logins', 0)->where('status', '=', '1')->where('isdeleted', '=', '0')->first();
    if($users){
      return response()->json(['status'=>true,'message'=>'Your account has been successfully created. Please provide information about your business now.','error'=>'','data'=>$users], 200);
    }
  } catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
  } catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
  }
}

public function getPlaceId(Request $request) {
   try{
    $businessName = urlencode(trim($request->business_address));
    if($businessName !=''){
    $url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=".$businessName."&inputtype=textquery&fields=formatted_address,name,rating,place_id&key=".env('GOOGLE_API_KEY');
    $json = file_get_contents($url);
    $data = json_decode($json, true);

      if($data['status']=='OK') {
        $Input = [];
        $Input['business_page_id'] = $data['candidates'][0]['place_id'];
        $Input['business_address_two'] = $data['candidates'][0]['formatted_address'];
        $Input['business_name_two'] = $data['candidates'][0]['name'];
        $Input['business_address'] = urlencode(trim($request->business_address));
        return response()->json(['status'=>true,'message'=>'Google Place Id found successfully','error'=>'','data'=>$Input], 200);
      } else {
        return response()->json(['status'=>false,'message'=>'Google place Id could not found for this business name so please enter a valid business name','error'=>'','data'=>''], 200);
      }
    }else {
      return response()->json(['status'=>false,'messagestatus'=>'Warning','message'=>'Business Address Not found','error'=>'','data'=>''], 400);
    }
  } catch(\Illuminate\Database\QueryException $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Sql query error','data'=>''], 401); 
  } catch(\Exception $e) {
    $errorClass = new ErrorsClass();
    $errors = $errorClass->saveErrors($e);
    return response()->json(['status'=>false,'message'=>'','error'=>'Undefined variable error','data'=>''], 401);
  }
}

function RemoveSpecialChapr($string){
    $title = str_replace( array( '\'', '"', ',', ';', '<', '>', ':', '_', '-', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '.', '?', '/', '`', '|', '[', ']', '{', '}' ), '$', $string);
        return $title;
  }

public function guard()
{
  return Auth::guard();
}

}
