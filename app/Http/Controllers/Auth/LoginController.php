<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated($request, $user)
    {  
      return redirect()->intended('/');
      //return redirect()->intended('/setting');
        /*if( $user->isdeleted=='0' ) {
            if( $user->status=='1' ) {
                  if($user->role=='admin') {
                    return redirect()->intended('/setting');  
                  } else {
                        if($user->isapproved=='1'){
                            return redirect()->intended('/customerdashboard');
                        } else {
                            return redirect()->intended('/login')->with('error','Your account is not approved. Please contact with website admin to approved your account.');
                        }
                  }

            } else {
                Auth::logout();
                    return redirect()->intended('/login')->with('error','Your account is not active. Please contact with website admin to activate your account.');
            } 

       } else {
            Auth::logout();
                return redirect()->intended('/login')->with('error','Your account is deleted. Please contact with website admin to restore your account.');
       }*/      

    }
}
